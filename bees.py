import multiprocessing as mp
import queue

from .util import *

def worker(input, output):
	for path in iter(input.get, 'stop'):
		result = worker_process(path)
		
		
		
		output.put(result)

class Queen:
	def __init__(self, nworkers):
		self.tasks = mp.Queue()
		self.results = mp.Queue()
		self.nworkers = nworkers
		self.started = False
	
	def start(self):
		for _ in range(self.nworkers): # Create worker processes
			mp.Process(target=worker, args=(tasks, results)).start()
		self.started = True
	
	def stop(self): # Tell all the workers to shut down
		for _ in range(self.nworkers):
			self.tasks.put('stop')
		self.started = False
	
	def run(self, data):
		if not self.started: self.start() # Make sure the workers are running
		
		for val in data: # Give them their starting tasks
			self.tasks.put(val)
		
		while True:
			try:
				result = self.results.get(
		while results.get()[0] != ResultType.success and not self.tasks.empty(): # Block until we get success