from functools import lru_cache
import colorama as cr

from util import *

normal_table = '┼┬┤┐┴─┘#├┌│#└'
constraint_table = '╬╦╣╗╩═╝#╠╔║#╚'
endpoint_table = '↗↑→↗↓↑↘#←↖→#↙'

def fancy_connections(n, constraint=False, endpoint=False):
	top = (n.y == 0)
	bottom = (n.y == n.parent.height-1)
	left = (n.x == 0)
	right = (n.x == n.parent.width-1)
	
	index = (1 if top else 0) + (2 if right else 0) + (4 if bottom else 0) + (8 if left else 0)
	if constraint:
		return constraint_table[index]
	elif endpoint:
		return endpoint_table[index]
	else:
		return normal_table[index]

def cube_symbol(n):
	if n is None: # This is just a normal hexagon
		return '⬣' if fancy else '@'
	else:
		return chr(ord('①')+n-1) if fancy else str(n)

def draw_node(n, fancy=True):
	if n.type == NodeType.normal:
		return fancy_connections(n) if fancy else '+'
	elif n.type == NodeType.cube:
		return cube_symbol(n.number)
	elif n.type == NodeType.start:
		return '○' if fancy else 'O'
	elif n.type == NodeType.end:
		return fancy_connections(n, endpoint=True) if fancy else 'X'
	elif n.type == NodeType.constraint:
		return fancy_connections(n, constraint=True) if fancy else '*'
	else:
		return '?'

def draw_cell(c, fancy=True):
	if c.type == CellType.normal:
		return '   '
	elif c.type == CellType.block:
		return '▪▪▪' if fancy else '+++'
	elif c.type == CellType.antiblock:
		return '▫▫▫' if fancy else 'ooo'
	elif c.type == CellType.brick:
		return '▐█▌' if fancy else '[#]'
	elif c.type == CellType.antibrick:
		return ' ✸ ' if fancy else ' * '
	elif c.type == CellType.anticube:
		return ' ⅄ ' if fancy else ' Y '
	elif c.type == CellType.tri:
		if c.number == 1:
			return ' ▴ ' if fancy else ' ^ '
		elif c.number == 2:
			return '▴ ▴' if fancy else '^ ^'
		else:
			return '▴▴▴' if fancy else '^^^'
	else:
		return '???'

def blockchr(fancy=True, anti=False, rotate=False):
	if anti:
		if rotate:
			return '◇' if fancy else 'c'
		else:
			return '▫' if fancy else 'o'
	else:
		if rotate:
			return '◆' if fancy else 'a'
		else:
			return '▪' if fancy else '+'

miniblock_table = ' ▗▖▄▝▐▞▟▘▚▌▙▀▜▛█'
def draw_miniblock_ind(top_left, top_right, bottom_left, bottom_right, fancy=True):
	if not fancy: return '#' # Don't even bother
	return miniblock_table[
		8 if top_left else 0 +
		4 if top_right else 0 +
		2 if bottom_left else 0 +
		1 if bottom_right else 0
	]

def draw_miniblock(shape, w, h, fancy=True):
	if h % 2: # Need to add interior
		for line in shape:
			line.append(False)
	if w % 2: # Need to add exterior
		shape.append([False]*h)
	
	lines = []
	for y in range(h, step=2):
		tmp = []
		for x in range(w, step=2):
			tmp.append(draw_miniblock_ind(
				shape[x][y],
				shape[x+1][y],
				shape[x][y+1],
				shape[x+1],[y+1]
			))
		lines.append(''.join(tmp).center(3))
	
	while len(lines) <3:
		lines.append('   ')
	
	return lines

def draw_block(b, fancy=True, anti=False):
	if not b: return ('   ', '   ', '   ') # Treat an empty block as a plain cell
	ch = blockchr(fancy, anti, b.rotate)
	
	w = b.width
	h = b.height
	
	if w > 3 or h > 3: # Too large to display
		return draw_miniblock(b.layout, w, h, fancy) # Draw a smaller block diagram, with four blocks to the character
	#	return [[ch for i in range(3)] for j in range(3)] # Just make a solid square out of nine of 'em
	
	return [ ''.join([(ch if i<w and j<h and b.layout[i][j] else ' ') for i in range(3)]) for j in range(3) ]

def draw_block_alt(b): # For testing the above
	for y in range(len(b.layout[0])):
		for x in range(len(b.layout)):
			print('#' if b.layout[x][y] else '-', end='')
		print()

def default_edge(vert=False, fancy=True, constraint=False):
	if constraint:
		if vert:
			return '║' if fancy else '$'
		else:
			return '═' if fancy else '='
	else:
		if vert:
			return '│' if fancy else '|'
		else:
			return '─' if fancy else '-'

def draw_edge(e, vert=False, fancy=True):
	if e.type == EdgeType.normal:
		return default_edge(vert, fancy)
	elif e.type == EdgeType.cube:
		return cube_symbol(e.number)
	elif e.type == EdgeType.blocked:
		return ' '
	elif e.type == EdgeType.constraint:
		return default_edge(vert, fancy, constraint=True)

def draw_color(c):
	if c == Color.colorless:
		return cr.Style.RESET_ALL
	elif c == Color.black:
		return cr.Fore.BLACK
	elif c == Color.red:
		return cr.Fore.RED
	elif c == Color.green:
		return cr.Fore.GREEN
	elif c == Color.yellow:
		return cr.Fore.YELLOW
	elif c == Color.blue:
		return cr.Fore.BLUE
	elif c == Color.pink:
		return cr.Fore.MAGENTA
	elif c == Color.cyan:
		return cr.Fore.CYAN
	elif c == Color.white:
		return cr.Fore.WHITE
	return cr.Fore.RESET

def reset_fg():
	return cr.Fore.RESET

def highlight_bg():
	return cr.Back.BLUE

def cancel_bg():
	return cr.Back.GREEN

def error_bg():
	return cr.Back.RED

def reset_bg():
	return cr.Back.RESET

def draw_graph(g, fancy=True, highlight=None, cancel=None, error=None):
	width = 2 * g.width - 1 # 1 per node, 1 per cell, -1 because one more node than cell
	height = 2 * g.height - 1 # Same
	diagram = [[None for y in range(height)] for x in range(width)]
	
	if highlight is None: highlight = set()
	if cancel is None: cancel = set()
	if error is None: error = set()
	
	# Now fill it
	for x in range(g.width):
		for y in range(g.height):
			# Step 1: draw the node at (x, y) -> (2x, 2y)
			diagram[2*x][2*y] = g.node((x, y))
			# Step 2: draw the edge right from (x, y) -> (2x+1, 2y)
			if x < g.width-1:
				diagram[2*x+1][2*y] = g.node((x, y)).edgeto(g.node((x+1, y)))
			# Step 3: draw the edge down from (x, y) -> (2x, 2y+1)
			if y < g.height-1:
				diagram[2*x][2*y+1] = g.node((x, y)).edgeto(g.node((x, y+1)))
			# Step 4: draw the cell at (x, y) -> (2x+1, 2y+1)
			if x < g.width-1 and y < g.height-1:
				diagram[2*x+1][2*y+1] = g.cell((x, y))
	
	# Turn these into strings
	strings = [ g.header[0] ]
	
	highlight_mode = False
	need_reset = False
	
	for y in range(height):
		if y%2 == 0: # We're on a "narrow" (edge) row
			lines = [],
		else: # We're on a "wide" (cell) row
			lines = [], [], []
		
		for x in range(width):
			
			# First decide which background color we need.
			if diagram[x][y] in cancel: # This was an error cancelled out by an anticube
				need_reset = True # Remember to reset after this one
				for line in lines: line.append(cancel_bg())
			
			elif diagram[x][y] in error: # This is actually a legitimate error
				need_reset = True
				for line in lines: line.append(error_bg())
			
			elif diagram[x][y] in highlight: # This one should be highlighted for whatever reason
				if not highlight_mode: # We aren't currently highlighting? We should be!
					highlight_mode = True
					for line in lines: line.append(highlight_bg())
			
			elif highlight_mode: # We don't need any special background, but we're still highlighting from before
				highlight_mode = False
				for line in lines: line.append(reset_bg()) # So stop doing that
			
			if y%2 == 0 and x%2 == 0: # This is a node
				lines[0].append(draw_node(diagram[x][y], fancy))
			
			elif y%2 == 1 and x%2 == 1: # This is a cell				
				if diagram[x][y].type == CellType.block or diagram[x][y].type == CellType.antiblock:
					for line in lines: line.append(draw_color(diagram[x][y].color))
					
					anti = (diagram[x][y].type == CellType.antiblock)
					diag = draw_block(diagram[x][y].block, fancy, anti)
					for line, d in zip(lines, diag): line.append(d)
					
					for line in lines: line.append(reset_fg())
				else:
					lines[1].append(draw_color(diagram[x][y].color))
					
					lines[0].append('   ')
					lines[1].append(draw_cell(diagram[x][y], fancy))
					lines[2].append('   ')
					
					lines[1].append(reset_fg())
			
			elif y%2 == 0 and x%2 == 1: # This is a horizontal edge
				c = (diagram[x][y].type == EdgeType.constraint) # Is this a constraint?
				
				lines[0].append(default_edge(False, fancy, c))
				lines[0].append(draw_edge(diagram[x][y], False, fancy))
				lines[0].append(default_edge(False, fancy, c))
			
			elif y%2 == 1 and x%2 == 0: # This is a vertical edge
				c = (diagram[x][y].type == EdgeType.constraint) # Is this a constraint?
				
				lines[0].append(default_edge(True, fancy, c))
				lines[1].append(draw_edge(diagram[x][y], True, fancy))
				lines[2].append(default_edge(True, fancy, c))
			
			if need_reset: # We did some formatting which now needs undoing
				for line in lines: line.append(reset_bg())
				need_reset = False
				highlight_mode = False
		
		if highlight_mode: # Turn it off at the end of each line anyway
			for line in lines: line.append(reset_bg())
			highlight_mode = False
		
		strings.extend(''.join(line) for line in lines)
	
	return '\n'.join(h+s for h, s in zip(g.header[1], strings)) # Now join all the lines with \n and send them out

def draw_path(p, fancy=True): # Little wrapper function
	g = p.parent
	highlight, cancel, error = p.highlight_cancel_error()
	return draw_graph(g, fancy, highlight, cancel, error)

def draw_region(r, fancy=True):
	g = r.parent.parent
	return draw_graph(g, fancy, r.cells)
