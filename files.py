import json

from util import *

parse = {
	'colorless' : Color.colorless,
	'black' : Color.black,
	'red' : Color.red,
	'green' : Color.green,
	'yellow' : Color.yellow,
	'blue' : Color.blue,
	'magenta' : Color.pink,
	'cyan' : Color.cyan,
	'white' : Color.white,
	
	'empty' : CellType.normal,
	'shape' : CellType.block,
	'unshape' : CellType.antiblock,
	'brick' : CellType.brick,
	'star' : CellType.antibrick,
	'anti' : CellType.anticube,
	'tri' : CellType.tri,
	
	'on' : EdgeType.normal,
	'ecube' : EdgeType.cube,
	'off' : EdgeType.blocked,
	'econs' : EdgeType.constraint,
	
	'normal' : NodeType.normal,
	'ncube' : NodeType.cube,
	'start' : NodeType.start,
	'end' : NodeType.end,
	'ncons' : NodeType.constraint
}

unparse = {v:k for k,v in parse.items()} # Extend the parse table so it goes backwards too

def load(fname):
	with open(fname, 'r') as f:
		data = json.load(f)
	
	width, height = data['dimensions']['x'], data['dimensions']['y']
	out = Graph(width, height)
	
	points = data['points']
	for point in points:
		x, y = point['x'], point['y']
		number = int(point['number']) if 'number' in point else None
		type = parse[point['type']] if 'type' in point else NodeType.normal
		
		result = out.node((x, y))
		result.number = number
		result.type = type
	
	cells = data['cells']
	for cell in cells:
		x, y = cell['x'], cell['y']
		color = parse[cell['color']] if 'color' in cell else Color.colorless
		number = int(cell['number']) if 'number' in cell else None
		type = parse[cell['type']] if 'type' in cell else CellType.normal
		block = Block(cell['block']['layout'], cell['block']['rotate']) if 'block' in cell else None
		
		result = out.cell((x, y))
		result.color = color
		result.number = number
		result.type = type
		result.block = block
	
	edges = data['edges']
	for edge in edges:
		x1, y1 = edge['x1'], edge['y1']
		x2, y2 = edge['x2'], edge['y2']
		number = int(edge['number']) if 'number' in edge else None
		type = parse[edge['type']] if 'type' in edge else None
		
		result = out.edge((x1, y1), (x2, y2))
		result.number = number
		result.type = type
	
	if 'path' not in data: # We're done here
		return out
	
	inpath = data['path']['nodes']
	nodes = []
	for node in inpath:
		nodes.append(out.node((node['x'], node['y'])))
	path = Path(out, nodes)
	
	path.calculate_edges()
	path.calculate_regions()
	if data['path']['final']:
		path.finalize()
	
	return path

def dump(inp, fname):
	data = {}
	
	if isinstance(inp, Graph):
		graph = inp
	elif isinstance(inp, Path):
		graph = inp.parent
	else:
		raise ValueError('need Graph or Path')
	
	data['dimensions'] = {'x':graph.width, 'y':graph.height}
	
	data['points'] = []
	for p in graph.iternodes():
		if p.type != NodeType.normal:
			point = {'x':p.x, 'y':p.y}
			
			point['type'] = unparse[p.type]
			if p.number is not None: point['number'] = p.number
			
			data['points'].append(point)
	
	data['cells'] = []
	for c in graph.itercells():
		if c.type != CellType.normal:
			cell = {'x':c.x, 'y':c.y}
			
			cell['type'] = unparse[c.type]
			if c.color != Color.colorless: cell['color'] = unparse[c.color]
			if c.number is not None: cell['number'] = c.number
			if c.block is not None: cell['block'] = {'rotate':c.block.rotate, 'layout':c.block.layout}
			
			data['cells'].append(cell)
	
	data['edges'] = []
	for e in graph.iteredges():
		if e.type != EdgeType.normal:
			n1, n2 = tuple(e.ends)
			edge = {'x1':n1.x, 'y1':n1.y, 'x2':n2.x, 'y2':n2.y}
			
			edge['type'] = unparse[e.type]
			if e.number is not None: edge['number'] = e.number
			
			data['edges'].append(edge)
	
	if graph != inp: # We were given a path!
		data['path'] = {'final':inp.final, 'nodes':[]}
		for n in inp.nodes:
			data['path']['nodes'].append({'x':n.x, 'y':n.y})
	
	with open(fname, 'w') as f:
		json.dump(data, f)
