import colorama as cr

from util import *

p = [ (0,0), (1,0), (2,0), (2,1), (2,2), (2,3), (2,4), (3,4), (4,4) ]

puzzle = Graph(5, 5)
path = Path(puzzle, mode='init')
for t in p:
	path.takestep(puzzle[t])

def pause():
	input()

def demo(title, objs):
	print(title)
	puzzle.reset_highlight()
	for obj in objs:
		obj.highlight = True
	print(puzzle.draw())
	pause()

cr.init()

pool = set(puzzle.itercells())
out = set()

demo('Starting path', path.nodes + path.edges)

while pool: # There are still untraversed cells
	
	demo('Current pool', pool)
	
	queue = [] # (Actually more of a stack, but the principle stands)
	region = set()
	queue.append(pool.pop()) # Take an arbitrary cell from the pool and put it on the stack
	
	demo('Starting cell', (queue[0],))
	
	while queue: # Floodfill: keep adding untraversed adjacent cells to this list, then exploring them
		c = queue.pop()
		region.add(c)
		
		demo('\tSpreading', (c,))
		
		for link in c.edges:
			demo('\t\tExamining edge', (link,))
			if link not in path.edges: # If this edge has been drawn we can't cross it
				other = link.across(c)
				if not other: continue # This isn't actually a useful edge
				if other not in pool: continue # Already flooded
				
				demo('\t\tCrossing to', (other,))
				
				queue.append(other)
				pool.remove(other)
		
		demo('\tCurrent state', region)
	
	demo('Region filled', region)
	
	out.add(Region(region))