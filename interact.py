from pyfiglet import Figlet
import colorama as cr
import sys

import readline
import atexit
import os.path

from util import *
from solver import solve
import draw
import files

witness_history = os.path.join(os.path.expanduser("~"), ".witness_history")
try:
	readline.read_history_file(witness_history)
	readline.set_history_length(50)
except FileNotFoundError:
	pass
atexit.register(readline.write_history_file, witness_history)

def parse_letter(letter, reverse=False):
	if not reverse:
		if 'A' <= letter <= 'Z':
			return True, ord(letter) - ord('A')
		elif 'a' <= letter <= 'z':
			return False, ord(letter) - ord('a')
		else:
			return None, None
	else:
		if 'A' <= letter <= 'Z':
			return True, ord('Z') - ord(letter)
		elif 'a' <= letter <= 'z':
			return False, ord('z') - ord(letter)
		else:
			return None, None

def is_int(s):
	try:
		int(s)
		return True
	except ValueError:
		return False

node_type_parse = {
	'normal' : NodeType.normal,
	'cube' : NodeType.cube,
	'required' : NodeType.cube,
	'hex' : NodeType.cube,
	'hexagon' : NodeType.cube,
	'start' : NodeType.start,
	'circle' : NodeType.start,
	'end' : NodeType.end
}

cell_type_parse = {
	'normal' : CellType.normal,
	'block' : CellType.block,
	'shape' : CellType.block,
	'tetris' : CellType.block,
	'antiblock' : CellType.antiblock,
	'noshape' : CellType.antiblock,
	'brick' : CellType.brick,
	'square' : CellType.brick,
	'antibrick' : CellType.antibrick,
	'sun' : CellType.antibrick,
	'star' : CellType.antibrick,
	'sunburst' : CellType.antibrick,
	'starburst' : CellType.antibrick,
	'y' : CellType.anticube,
	'anticube' : CellType.anticube,
	'solver' : CellType.anticube,
	'tri' : CellType.tri
}

edge_type_parse = {
	'normal' : EdgeType.normal,
	'on' : EdgeType.normal,
	'cube' : EdgeType.cube,
	'required' : EdgeType.cube,
	'hex' : EdgeType.cube,
	'hexagon' : EdgeType.cube,
	'off' : EdgeType.blocked,
	'blocked' : EdgeType.blocked
}

color_parse = {
	'colorless' : Color.colorless,
	'default' : Color.colorless,
	'black' : Color.black,
	'red' : Color.red,
	'green' : Color.green,
	'yellow' : Color.yellow,
	'gold' : Color.yellow,
	'blue' : Color.blue,
	'pink' : Color.pink,
	'magenta' : Color.pink,
	'purple' : Color.pink,
	'cyan' : Color.cyan,
	'azure' : Color.cyan, # Not actually azure...this is customary though
	'white' : Color.white
}

global_highlighting = None

def command_set(puzzle, command):
	(xi, x), (yi, y) = parse_letter(command[0][0], False), parse_letter(command[0][1], True)
#	print('dbg: {} {} {} {}'.format(xi, x, yi, y))
	
	command[1:] = map(str.lower, command[1:])
	type = command[1]
	
	if xi is None or yi is None or x >= puzzle.width or y >= puzzle.height:
		print('Invalid coordinates: {}, {}'.format(x, y))
		return
	
	if xi and yi: # Two capital letters: this is a NODE
		target = puzzle[x, y]
		if type not in node_type_parse:
			print('Unrecognized node type: {}'.format(type))
			return
		target.type = node_type_parse[type]
	elif not xi and not yi: # Two lowercase letters: this is a CELL
		target = puzzle.cell((x, y))
		for val in command[1:]:
			if val in cell_type_parse:
				target.type = cell_type_parse[val]
			elif val in color_parse:
				target.color = color_parse[val]
			elif is_int(val):
				target.number = int(val)
			else:
				print('Unrecognized cell value: {}'.format(type))
				return
	else: # One lowercase and one uppercase: this is an EDGE
		t1 = puzzle[x, y]
		if xi: # This edge is vertical
			t2 = puzzle[x, y+1]
		else: # Horizontal
			t2 = puzzle[x+1, y]
		target = t1.edgeto(t2)
		
		if type not in edge_type_parse:
			print('Unrecognized edge type: {}'.format(type))
			return
		target.type = edge_type_parse[type]

def command_block(puzzle, tgt, w, h, rotate, *args):
	w, h = int(w), int(h)
	(xi, x), (yi, y) = parse_letter(tgt[0], False), parse_letter(tgt[1], True)
	rotate = (rotate == 'on')
	if xi or yi:
		print('Bad cell')
		return
	c = puzzle.cell((x, y))
	
	layout = [[False for i in range(h)] for j in range(w)]
	for arg in args:
		x = ord(arg[0])-ord('A')
		y = ord(arg[1])-ord('A')
		print('\tSetting {}, {} from {}'.format(x, y, arg))
		layout[x][y] = True
	
	c.block = Block(layout, rotate)

def parse_command(puzzle, command, d):
	global global_highlighting
	
	if not command:
		return
	
	if command[0] == 'solve':
		path = solve(puzzle)
		if not path:
			print('No solutions found.')
			return
		print('Path saved as "path"')
		d['path'] = path
		for i, r in enumerate(path.regions):
			d['r_'+str(i)] = r
		print('Regions saved up to "r_{}"'.format(i))
	
	elif command[0] == 'highlight':
		global_highlighting = set()
		
		for word in command[1:]:
			target = d[word]
			
			if isinstance(target, Path):
				for node in target.nodes:
					global_highlighting.add(node)
				for edge in target.edges:
					global_highlighting.add(edge)
			
			elif isinstance(target, Region):
				for c in target.cells:
					global_highlighting.add(c)
		
	elif command[0] == 'set':
		command_set(puzzle, command[1:])
	
	elif command[0] == 'block':
		command_block(puzzle, *command[1:])
	
	elif command[0] == 'load':
		with open(command[1], 'r') as f:
			for line in f:
				parse_command(puzzle, line.split(), d)
	
	elif command[0] == 'optimize':
		puzzle.optimize()
	
	elif command[0] == 'save':
		files.dump(puzzle, command[1])
	
	elif command[0] == 'pause':
		display_path.pausemode = (command[1] == 'on')
		print('Pause turned {}'.format('on' if display_path.pausemode else 'off'))
	
	elif command[0] == 'quit':
		sys.exit(0)
	
	else:
		print('Not recognized')

def shell():
	txt = Figlet(font='chunky')
	
	print(txt.renderText('Witness'))
	w, h = (int(x.strip()) for x in input('Dimensions: ').split(','))
	
	puzzle = Graph(w, h) # Make the puzzle itself
	
	d = {}
	
	while True:
		print(draw.draw_graph(puzzle, fancy=True, highlight=global_highlighting))
		command = input('>').split()
		parse_command(puzzle, command, d)

if __name__ == '__main__':
	cr.init()
	shell()
