import os
import time

from util import *

searchmode = 'dfs'

class stack:
	def __init__(self):
		self._cont = []
	
	def put(self, v):
		self._cont.append(v)
	
	def get(self):
		return self._cont.pop()
	
	def __bool__(self):
		return bool(self._cont)

if searchmode == 'dfs':
	container = stack
else:
	import queue
	container = queue.Queue

def step(sofar):
	last = sofar.nodes[-1] # The node we're currently at
	
	if len(last.required) >= 2 and not any(e in sofar.edges for e in last.required): # We've made a terrible mistake—there are two required edges from this node, and we've approached it from a different one.
		return [] # Don't bother continuing, just give up and reflect on our mistakes
	
	if last.required.difference(sofar.edges): # Our choices are somewhat constrained here
		options = sorted(n for n in last.reqn if n not in sofar.nodes)
	else: # No constraints to worry about at the moment! :D
		options = sorted(n for n in last.neighbors if n not in sofar.nodes) # Choose all neighbors who have not already been visited
	
	if not options: return [] # Make sure not to infinite-loop
	
	out = [sofar] + [sofar.copy() for _ in options[1:]]
	for path, nextstep in zip(out, options):
		path.takestep(nextstep)
	
	return out

def solve(graph):
	start = [i for i in graph.iternodes() if i.type==NodeType.start]
	queue = container()
	
	for point in start:
		p = Path(graph, [point], 'init')
		queue.put(p)
	
	while queue:
		for path in step(queue.get()):
			
			if path.valid:
				if path.nodes[-1].type == NodeType.end: # We got to the end
					path.finalize()
										
					if path.valid:
						print('Done!')
						return path
					
					path.final = False
					
					# Have we messed up?
					if not any(1 for n in graph.iternodes() if n.type == NodeType.end and n not in path.nodes):
						continue # We have! No endpoints are reachable from the current position
					
				queue.put(path)
	
	print('No solutions exist :( ')
	return None

if __name__ == '__main__':
	from sys import argv
	import files
	
	print('Attempting to solve')
	inp = files.load(argv[1])
	out = solve(inp)
	if out:
		files.dump(out, argv[2])
