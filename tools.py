def all_same(items):
	it = iter(items)
	first = next(it, None)
	return all(x == first for x in it)