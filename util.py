from enum import Enum, unique
from functools import total_ordering
import colorama as cr
import itertools as it
import numpy as np

from tools import all_same

@unique
class NodeType(Enum):
	normal = 0
	cube = 1
	start = 2
	end = 3
	constraint = 4

@unique
class CellType(Enum):
	normal = 0
	block = 1
	antiblock = 2
	brick = 3
	antibrick = 4
	anticube = 5
	tri = 6

@unique
class EdgeType(Enum):
	normal = 0
	cube = 1
	blocked = 2
	constraint = 3 # Some other aspect of the puzzle means that this edge is required

@unique
class Color(Enum):
	colorless = -1
	black = 0
	red = 1
	green = 2
	yellow = 3
	blue = 4
	pink = 5
	cyan = 6
	white = 7
	
	def __lt__(self, other):
		return self.value < other.value

@unique
class ResultType(Enum):
	failed = 0,
	partial = 1,
	success = 2

class Block:
	def __init__(self, layout=[[]], rotate=False):
		self.layout = layout
		self.rotate = rotate
		self.area = sum(1 for row in layout for elem in row if elem) # Count how many filled squares there are
	
	@property
	def height(self):
		if not self.layout: return 0
		return len(self.layout[0])
	
	@property
	def width(self):
		return len(self.layout)
	
	def __bool__(self):
		return self.area > 0
	
	def __lt__(self, other): # For sorting largest to smallest
		return self.area < other.area
	
	def copy(self):
		new = [i.copy() for i in self.layout]
		return Block(new, self.rotate)
	
#	def cw(self): # Clockwise
#		return Block(list(zip(i.copy() for i in self.layout[::-1])), False) # Rotate an array clockwise using 'clever tricks'
	
#	def ccw(self): # Counterclockwise
#		return Block(list(zip(i.copy() for i in self.layout)[::-1]), False) # As above
	
#	def flip(self): # 180 degrees
#		return Block(list(list(i[::-1]) for i in self.layout[::-1]), False) # As above
	
	def rotated(self, angle):
		rot = np.rot90(self.layout, angle).tolist()
		return Block(rot, False)
	
#	def rotated(self, angle):
#		if angle == 0:
#			return self.copy()
#		elif angle == 1:
#			return self.cw()
#		elif angle == 2:
#			return self.flip()
#		elif angle == 3:
#			return self.ccw()
#		else:
#			raise ValueError('angle must be 0, 1, 2, or 3')
	
	def placeinto(self, other, place): # Attempt to place (self) into (other) at (place), returning either the result or None
		cx, cy = place
		
		# First, a sanity check
		if cx + self.width > other.width or cy + self.height > other.height: # Doesn't physically fit within confines!
			return None
		
		out = other.copy()
		for x in range(self.width):
			for y in range(self.height):
				if self.layout[x][y]:
					if out.layout[cx+x][cy+y]: return None # Conflict!!
					out.layout[cx+x][cy+y] = True
		
		return out
	
	def placeall(self, other): # Call placeinto at every possible location, stripping NULLs from the output set
		gen = (self.placeinto(other, (x, y)) for x in range(other.width - self.width + 1) for y in range(other.height - self.height + 1))
		# For every possible coordinate in the range, return the result if it is a valid Block
		return [result for result in gen if result is not None]
	
	def placeall_rot(self, other): # Wrapper to check for rotations as well
		if not self.rotate: return self.placeall(other)
		return list(it.chain.from_iterable(self.rotated(i).placeall(other) for i in range(4)))
	
	def theorem(self): # Check whether this current Block can obey the Theorem
		if not self.area: return True # Avoid going out of range if we don't have to
		
		w = len(self.layout)
		h = len(self.layout[0])
		queue = set(
			[ (x, 0) for x in range(w) ] +
			[ (0, y) for y in range(h) ] +
			[ (x, h-1) for x in range(w) ] +
			[ (w-1, y) for y in range(h) ]
		)
		seen = set()
		shouldsee = queue.copy()
		
		while queue:
			x, y = queue.pop() # Get an arbitrary element
			seen.add((x, y)) # We saw this one now
			neighbors = ((x+1, y), (x-1, y), (x, y+1), (x, y-1))
			
			for nx, ny in neighbors:
				if (nx > 0 and ny > 0 and nx < w-1 and ny < h-1) and (not (self.layout[x][y] and not self.layout[nx][ny])) and ((nx, ny) not in seen): # This neighbor is a valid point, it's not a filled -> empty move, and we haven't already seen it - good to go
					queue.add((nx, ny))
		
		return seen == shouldsee
	
	def placeall_rot_many(self, others): # Return the union of placeall_rot for many others
		return list(it.chain.from_iterable(self.placeall_rot(other) for other in others))
	
	def holds(self, others): # Can this block hold all others in the list? Return a list of valid solutions if so
		if isinstance(others, Block): others = [others] # Make sure we have a list
		current = [self] # "current" will hold a list of the free spaces in the grid at any given point
		for other in sorted(others, reverse=True): # Run through the blocks we need to place, from largest to smallest
			if not current: # There are no valid spaces left
				return False # So we can't do anything more
			current = other.placeall_rot_many(current) # Make a new set of possible placements from the last one
		return any(i is not None for i in current) # Return whether it can hold all of those

@total_ordering
class Element:
	count = 0
	
	def __init__(self, type, parent, number=None):
		self.type = type
		self.parent = parent
		self.number = number
		
		self.highlight = False
		
		self.id = Element.count
		Element.count += 1
	
	def __hash__(self): # Usually, these ids should be unique. But using integers instead of memory addresses allows them to be pickled and unpickled with their pointers remaining valid.
		return hash(self.id)
	
	def __eq__(self, other):
		return other and self.id == other.id
	
	def __ne__(self, other):
		return self.id != other.id
	
	def __lt__(self, other):
		return self.id < other.id

class Cell(Element):
	def __init__(self, type, parent, coord, color=Color.white, block=None):
		Element.__init__(self, type, parent)
		self.color = color
		self.block = block
		self.edges = set()
		self.vertices = set()
		
		self.x, self.y = coord
		self.name = chr(ord('A')+self.x) + chr(ord('Z')-self.y)
		
		self._cache_neighbors = None
	
	def __str__(self):
		return '<{}>'.format(self.name)
	
	def neighbors(self):
		if not self._cache_neighbors:
			self._cache_neighbors = set(i.across(self) for i in self.edges)
		return self._cache_neighbors

class Edge(Element):
	def __init__(self, type, parent):
		Element.__init__(self, type, parent)
		self.ends = set()
		self.sides = set()
		self._cache_across = {}
	
	def across(self, other): # Use memoization to speed this up if possible
		if other not in self._cache_across:
			try:
				if other in self.ends:
					v = list(self.ends - {other})[0] # An arbitrary element from self.ends which is not other
				elif other in self.sides:
					v = list(self.sides - {other})[0] # Ditto for sides
				else:
					v = None
			except IndexError: # The other side of this edge is nothingness
				v = None
			self._cache_across[other] = v
		return self._cache_across[other]
	
	def __str__(self):
		return '<{}>'.format('-'.join(i.name for i in self.ends))

class Node(Element):
	def __init__(self, type, parent, coord):
		Element.__init__(self, type, parent)
		self.connections = set()
		self.border = False
		
		self.x, self.y = coord
		self.name = chr(ord('A')+self.x) + chr(ord('Z')-self.y)
		
		self._cache_neighbors = None
		self._cache_corners = None
		self._cache_to = {}
		self._cache_required = None
		self._cache_reqn = None
	
	def __str__(self):
		return '<{}>'.format(self.name)
	
	@property
	def neighbors(self):
		if not self._cache_neighbors: self._cache_neighbors = set(i.across(self) for i in self.connections if i.type != EdgeType.blocked)
		return self._cache_neighbors
	
	@property
	def required(self): # Which edges do we absolutely need to include?
		if not self._cache_required: self._cache_required = set(i for i in self.connections if i.type == EdgeType.cube or i.type == EdgeType.constraint)
		return self._cache_required
	
	@property
	def reqn(self): # Required neighbors
		if not self._cache_reqn: self._cache_reqn = set(i.across(self) for i in self.required)
		return self._cache_reqn
	
	@property
	def corners(self):
		if not self._cache_corners: self._cache_corners = set.union(*(i.sides for i in self.connections))
		return self._cache_corners
	
	def empty_cache(self):
		self._cache_neighbors = None
		self._cache_corners = None
		self._cache_to = {}
		self._cache_required = None
		self._cache_reqn = None
	
	def edgeto(self, other): # Find the edge leading to another Node
		if other not in self._cache_to:
			self._cache_to[other] = None
			for edge in self.connections:
				if other in edge.ends:
					self._cache_to[other] = edge
					break
		return self._cache_to[other]

class Region:
	def __init__(self, parent, cells=None):
		self.parent = parent
		self.cells = cells if cells is not None else set()
		self.errors = set()
		self.closed = False
	
	def copy(self):
		new = Region(self.parent)
		new.cells = self.cells.copy()
	#	new.errors = self.errors.copy()
		new.closed = self.closed
		return new
	
	def __iter__(self):
		return (c for c in self.cells)
	
	def overlap(self, other): # These regions share a cell
		return self.cells & other.cells
	
	def asblock(self): # Turn this region into an equivalent block
		xmax = max(c.x for c in self.cells)
		xmin = min(c.x for c in self.cells)
		ymax = max(c.y for c in self.cells)
		ymin = min(c.y for c in self.cells)
		
		layout = [[True for i in range(ymax-ymin+1)] for j in range(xmax-xmin+1)]
		for c in self.cells:
			layout[c.x-xmin][c.y-ymin] = False
		
		return Block(layout)
	
	def check(self, nodes, edges):
		self.errors = set() # Discard old errors
		self.check_bricks()
		self.check_antibricks()
		self.check_block_area()
		self.check_blocks()
		self.check_cubes(nodes, edges)
		self.check_tris(edges)
		self.check_anticubes()
		return len(self.errors)
	
	def check_cubes(self, nodes, edges):
		free_edges = set.union(*(i.edges for i in self.cells)) - set(edges)
		self.errors |= { i for i in free_edges if i.type == EdgeType.cube }
		
		free_nodes = set.union(*(i.vertices for i in self.cells)) - set(nodes)
		self.errors |= { i for i in free_nodes if i.type == NodeType.cube }
	
	def check_bricks(self):
		l = sorted((i for i in self.cells if i.type == CellType.brick), key=lambda c:c.color) # Create a list of all bricks, sorted by color
		colors = sorted([set(g) for k, g in it.groupby(l, key=lambda c:c.color)], key=lambda g:len(g), reverse=True) # Now convert that to a list of sets of cells, sorted from most to least prominent
		if len(colors) <= 1: return # No issues here!
		for color in colors[1:]: # Otherwise...
			self.errors.update(color) # Consider all of the less-prominent colors to be errors, and mark them as such
	
	def check_antibricks(self):
		pairs = {c.color: c for c in self.cells if c.type == CellType.antibrick}
		for hue, example in pairs.items():
			num = sum(1 for c in self.cells if c.color == hue) # Count how many times this color appears
			if num != 2: # They must be paired!
				self.errors.add(example) # Pick only one as the example to highlight
	
	def check_block_area(self):
		real = len(self.cells) # The real area of this region
		pos = sum(i.block.area for i in self.cells if i.type == CellType.block)
		neg = sum(i.block.area for i in self.cells if i.type == CellType.antiblock)
		if real != pos - neg and pos != neg:
			self.errors.update(i for i in self.cells if i.type == CellType.block or i.type == CellType.antiblock) # If area doesn't work out, put ALL blocks and antiblocks into the error list
	
	def check_blocks(self): # Complicated -_-
		blocks = (i.block for i in self.cells if i.type == CellType.block)
		if not self.asblock().holds(blocks):
			self.errors.update(i for i in blocks)
	
	def check_tris(self, edges):
		for c in self.cells:
			if c.type != CellType.tri: continue # Don't bother running validation on other shapes
			n = sum(1 for e in c.edges if e in edges) # How many edges on this cell are in the path?
			if n != c.number:
				self.errors.add(c)
	
	def check_anticubes(self):
		self.cancellations = set() # Record which elements cancelled with which others, in case we want to display that. This is a set of (anticube, error cell) tuples.
		
		antis = {c for c in self.cells if c.type == CellType.anticube}
		while self.errors and antis:
			self.cancellations.add((antis.pop(), self.errors.pop())) # Remove an anticube and an error from the list
		
		while len(antis) > 1:
			self.cancellations.add((antis.pop(), antis.pop())) # In the end they can cancel each other out, since they count as errors
		
		self.errors |= antis # Any remaining ones become errors in their own right

class Graph:
	def __init__(self, width, height):
		self.width = width
		self.height = height
		self.nodes = [[Node(NodeType.normal, self, (x, y)) for y in range(height)] for x in range(width)]
		self.cells = [[Cell(CellType.normal, self, (x, y)) for y in range(height-1)] for x in range(width-1)]
		self.edges = {} # Will be populated soon, in build()
		
		self.build()
		self.optimize()
		self.makeheaders()
	
	def makeheaders(self): # For printing nicely
		caps = [chr(ord('A')+n) for n in range(self.width)]
		lower = [chr(ord('a')+n) for n in range(self.width-1)] + ['']
		h1 = ' '.join(letter for letters in zip(caps, lower) for letter in letters) # Put them all together with spaces in between
		
		caps = [chr(ord('Z')-n) for n in range(self.height)]
		lower = [chr(ord('z')-n) for n in range(self.height-1)] + ['']
		h2 = ' '.join(letter for letters in zip(caps, lower) for letter in letters)
		
		self.header = (h1, ' '+h2)
	
	def __getitem__(self, t): # Convenient alias for .node
		return self.node(t)
	
	def __setitem__(self, t, v): # Should seldom be needed, so less error checking
		x, y = t
		self.nodes[x][y] = v
	
	def __contains__(self, t):
		if isinstance(t, Element):
			return t.parent == self
		else:
			x, y = t
			return x>=0 and x<self.width and y>=0 and y<self.height
	
	def build(self):		
		for x in range(self.width):
			for y in range(self.height):
				if x < self.width - 1:
					c1 = self.cells[x][y] if y < self.height-1 else None
					c2 = self.cells[x][y-1] if y > 0 else None
					self._addedge(x, y, x+1, y, c1, c2)
				if y < self.height - 1:
					c1 = self.cells[x][y] if x < self.width-1 else None
					c2 = self.cells[x-1][y] if x > 0 else None
					self._addedge(x, y, x, y+1, c1, c2)
				
				if x < self.width - 1 and y < self.height - 1:
					self._connectcell(self.cells[x][y], x, y)
				
				if x==0 or x==self.width-1 or y==0 or y==self.width-1: # Mark which cells are on the border, for use in the Theorem
					self.nodes[x][y].border = True
	
	def optimize(self): # Put optimization checks here
		if not any(True for c in self.itercells() if c.type == CellType.anticube): # There are no anticubes in this puzzle, so we can do a bit of optimization
			for k, e in self.edges.items():
				if any(c is None for c in e.sides): break # One of the sides is null, so don't continue
				if e.type == EdgeType.normal and all(c.type == CellType.brick for c in e.sides) and not all_same(c.color for c in e.sides): # But if this is a normal node, and there are bricks on both sides, and they disagree...
					e.type = EdgeType.constraint # ...then put a constraint on this edge. It must be in the final solution.
					for n in e.ends:
						if n.type == NodeType.normal:
							n.type = NodeType.constraint # Same for its endpoints.
	
	def _addedge(self, x1, y1, x2, y2, c1, c2):
		new = Edge(EdgeType.normal, self)
		new.ends.add(self.nodes[x1][y1])
		self.nodes[x1][y1].connections.add(new)
		new.ends.add(self.nodes[x2][y2])
		self.nodes[x2][y2].connections.add(new)
		if c1 is not None:
			new.sides.add(c1)
			c1.edges.add(new)
		if c2 is not None:
			new.sides.add(c2)
			c2.edges.add(new)
		
		index = frozenset(
			( (x1, y1), (x2, y2) )
		)
		self.edges[index] = new
	
	def _connectcell(self, c, x, y):
		c.vertices.add(self.nodes[x][y])
		c.vertices.add(self.nodes[x+1][y])
		c.vertices.add(self.nodes[x][y+1])
		c.vertices.add(self.nodes[x+1][y+1])
	
	def node(self, t):
		x, y = t
		if x >= self.width:
			raise IndexError('attempted to access node with x-index {} > {}'.format(x, self.width-1))
		if y >= self.height:
			raise IndexError('attempted to access node with y-index {} > {}'.format(y, self.height-1))
		return self.nodes[x][y]
	
	def edge(self, start, end):
		index = frozenset( (start, end) )
		if index not in self.edges:
			raise KeyError('no edge found between {} and {}'.format(start, end))
		return self.edges[index]
	
	def cell(self, t):
		x, y = t
		if x >= self.width-1:
			raise IndexError('attempted to access cell with x-index {} > {}'.format(x, self.width-2))
		if y >= self.height-1:
			raise IndexError('attempted to access cell with y-index {} > {}'.format(y, self.height-2))
		return self.cells[x][y]
	
	def itercells(self):
		return (c for line in self.cells for c in line)
	
	def iternodes(self):
		return (n for line in self.nodes for n in line)
	
	def iteredges(self):
		return (e for k, e in self.edges.items())

class Path:
	def __init__(self, parent, nodelist=None, mode=None):
		self.parent = parent
		self.nodes = nodelist if nodelist is not None else []
		self.edges = []
		self.regions = None
		self.final = False
		
		if mode=='final':
			self.finalize()
		elif mode=='init':
			self.init_regions()
	
	def copy(self):
		new = Path(self.parent)
		if self.nodes: new.nodes = self.nodes.copy()
		if self.edges: new.edges = self.edges.copy()
		if self.regions: new.regions = { r.copy() for r in self.regions }
		return new
	
	def finalize(self):
		self.final = True
	
	def calculate_edges(self):
		self.edges = []
		it = None
		
		for n in self.nodes: # Run through the list and find edges
			if it is not None:
				line = it.edgeto(n)
				self.edges.append(line)
			it = n
	
	def init_regions(self): # Make a single region holding the whole thing
		self.regions = { Region(self, set(self.parent.itercells())) }
	
	def divide_regions(self, pool): # Helper function: floodfill to find traversable regions
		out = set()
		
		while pool: # There are still untraversed cells
			queue = [] # (Actually more of a stack, but the principle stands)
			region = set()
			queue.append(pool.pop()) # Take an arbitrary cell from the pool and put it on the stack
			
			while queue: # Floodfill: keep adding untraversed adjacent cells to this list, then exploring them
				c = queue.pop()
				region.add(c)
				
				for link in c.edges:
					if link not in self.edges: # If this edge has been drawn we can't cross it
						other = link.across(c)
						if not other: continue # This isn't actually a useful edge
						if other not in pool: continue # Already flooded
						queue.append(other)
						pool.remove(other)
			
			out.add(Region(self, region))
		
		return out
	
	def calculate_regions(self): # Determine bounded areas all in one run
		pool = set(self.parent.itercells())
		self.regions = self.divide_regions(pool)
	
	def update_regions(self, loc, prev, edge): # Update the region list based on only the most recent edge added
		if not (loc.border and not prev.border): return # Little speedup: we could only have made new regions if we went from a center to an edge node
		
		cut = tuple( r for r in self.regions if not r.closed and any(c in r for c in edge.sides) )
		pool = set()
		
		for r in cut:
			self.regions.remove(r)
			pool |= r.cells
		
		for r in self.regions: # Regions not affected by this cut should be considered closed.
			r.closed = True
		
		self.regions |= self.divide_regions(pool) # Split the last region in two
	
	def takestep(self, step):
		self.nodes.append(step)
		if len(self.nodes) > 1:
			prev = self.nodes[-2]
			if self.edges is None: self.edges = []
			e = prev.edgeto(step)
			self.edges.append(e)
			self.update_regions(step, prev, e)
	
	def __bool__(self):
		return self.edges is not None
	
	@property
	def valid(self):
		if self.regions is None:
			self.calculate_regions()
		
		return not self.errors
	
	@property
	def errors(self):
		out = set()
		
		for i in self.regions:
			if i.closed or self.final:
				i.check(self.nodes, self.edges)
				out |= i.errors
		
		return out
	
	def highlight_cancel_error(self): # Return sets for highlight, cancel, and error
		highlight = set().union(self.nodes, self.edges)
		cancel = set()
		error = set()
		
		for r in self.regions:
			if r.closed or self.final:
				r.check(self.nodes, self.edges)
				cancel |= { i for sublist in r.cancellations for i in sublist }
				error |= r.errors
		
		return highlight, cancel, error #...I'm not sure what I was expecting
